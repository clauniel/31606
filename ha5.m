%4 zeros at normalized frequency of 0.1
derp = poly([1*exp(i*0.1*pi),exp(i*0.3*pi),exp(-i*0.1*pi),exp(-i*0.3*pi)])
figure(1)
zplane(derp,[])
freqz(derp,[1 0 0 0 0])
% add poles closing in on the zeros
%pole1 = poly(0.5*[exp(i*0.1*pi),exp(i*0.1*pi),exp(i*0.1*pi),exp(i*0.1*pi)])
%freqz(derp,pole1)