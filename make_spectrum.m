function [ Y, freq ] = make_spectrum( signal, fs )
%MAKE_SPECTRUM Returns Y, the spectrum values at times in freq
%   Detailed explanation goes here
%Compute the spectrum (complex valued)
Y = fft( signal );

% The fft needs to be scaled in order to give a physically plausible
% scaling ( why? )
Y = Y/length(Y);
  %NOTE: Don't do this when ifft is wanted, there length(Y)/2 must be used
  %instead

% Frequency vector
T = length(signal)/fs; %signal sample time
delta_f = 1/T; %frequency resolution
freq = [0:delta_f:fs/2-delta_f,-fs/2:delta_f:0-delta_f]; %fft spits out DC,positive frequencies and then negative freqs

end

