function [ output_args ] = plot_period( ts, te, signal, t, varargin )
%PLOT_PERIOD Plots the specified period of a signal in the current figure, given start time, end
%time, signal and time vector
%   ts: start time in [t]
%   te: end time in [t]
%   signal: The signal
%   t: time vector
%   optional color/style argument for plot
%   length(t)==length(signal) is assumed
temp_t = t(t>ts & t<te); %time vector for plot
temp = find(t>ts & t<te); 
i_start = temp(1);%indices in s corresponding to temp_t
i_stop = temp(end);
if nargin<=4
    plot(temp_t,signal(i_start:i_stop)) %plot without extras
else
    plot(temp_t,signal(i_start:i_stop),varargin{:}) %plot with extras
end

end

