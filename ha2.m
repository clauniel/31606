T_tot = 0.1;
fs = 1/0.002*15;
f = 1/0.002;
t = [0:1/fs:T_tot];
T0 = 0.01;
num_ones = T0*fs;
boxcar = [ones(1,num_ones)/T0,zeros(1,length(t)-num_ones)]
plot(t,boxcar)
ramp = [t(1:num_ones),zeros(1,length(t)-num_ones)]
figure(2)
plot(t,ramp)
sine = [sin(2*pi*f*t(1:num_ones)),zeros(1,length(t)-num_ones)];
figure(3)
plot(t,sine)

rect_rect = conv(boxcar,boxcar)
figure(4)
plot(t,rect_rect(1:length(t)))
ramp_rect = conv(ramp,boxcar);
plot(t,ramp_rect(1:length(t)))
sine_sine = conv(sine,sine);
plot(t,sine_sine(1:length(t)))
rect_sine = conv(boxcar,sine);
plot(t,rect_sine(1:length(t)))
ramp_sine = conv(ramp,sine);
plot(t,ramp_sine(1:length(t)))

%% filtering
T_tot = 0.05;
order = 21;
fs = 10e3;
h = [zeros(1,T_tot*fs),ones(1,order)/order,zeros(1,T_tot*fs-order+1)];
t = [-T_tot:1/fs:T_tot];
stem(t,h)
hold off
[ts,s476] = sine(1,100,0,10e3,0.05)
plot(ts,s476,'b')
hold on
tconv = [-0.1:1/fs:0.05];
s476h = conv(s476,h);
plot(tconv,s476h,'g')

%% rooms
delay_samples = 20;
alpha = 0.5;
h_ir = [1,zeros(1,delay_samples),alpha];
derp = conv(minime,h_ir')