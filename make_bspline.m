function [ Y ] = make_bspline( order, n )
%MAKE_BSPLINE makes a b-spline of order and length n
%   Detailed explanation goes here
h0 = ones(1,n);
Y = h0;
while( order > 0 ) 
    Y = conv(Y,h0);
    order = order-1;
end
Y = Y/norm(Y,1);
end

