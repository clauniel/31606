function [ out ] = fconv( a, b )
%FCONV convolves two inputs in the frequency domain
%   Detailed explanation goes here
lfinal = length(a)+length(b)-1;
a = [a,zeros(1,lfinal-length(a))];
b = [b,zeros(1,lfinal-length(b))];
A = fft(a);
B = fft(b);
OUT = A.*B;
out = ifft(OUT);
end

