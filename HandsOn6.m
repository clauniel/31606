%% Hands-on 6 - 2014-10-15

clear all
clc

%% 1 Impulse response and frequency response

%% 1.1 Placing poles

%1. Pole placements
%2. Frequency response
%3. Impulse response

%% Recursive filters with a single real pole

%generate transfer function denominators
h1 = poly([0.3]);    %gamma =  0.3
h2 = poly([0.95]);   %gamma =  0.95
h3 = poly([1.05]);   %gamma =  1.05
h4 = poly([-0.95]);  %gamma = -0.95

figure %plot z-plane pole diagrams
subplot(2,2,1)
zplane(1, h1);
title('\gamma = 0.3')
subplot(2,2,2)
zplane(1, h2);
title('\gamma = 0.95')
subplot(2,2,3)
zplane(1, h3);
title('\gamma = 1.05')
subplot(2,2,4)
zplane(1, h4);
title('\gamma = -0.95')

%generate transfer function frequency response
[H1, w1] = freqz(1, h1, 512);
[H2, w2] = freqz(1, h2, 512);
[H3, w3] = freqz(1, h3, 512);
[H4, w4] = freqz(1, h4, 512);

figure %plot magnitude and phase response
subplot(2,1,1)
plot(w1/pi, mag2db(abs(H1)), 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(w2/pi, mag2db(abs(H2)), 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(w3/pi, mag2db(abs(H3)), 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
plot(w4/pi, mag2db(abs(H4)), 'Color', [0.25 0 1.00], 'LineWidth', 1);
hold on
title('Magnitude Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Magnitude [dB]')
p = legend('\gamma = 0.3','\gamma = 0.95','\gamma = 1.05','\gamma = -0.95', 1);
set(p,'FontSize', 8);
subplot(2,1,2)
plot(w1/pi, (180/pi)*angle(H1), 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(w2/pi, (180/pi)*angle(H2), 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(w3/pi, (180/pi)*angle(H3), 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
plot(w4/pi, (180/pi)*angle(H4), 'Color', [0.25 0 1.00], 'LineWidth', 1);
hold on
ylim([-100 100])
title('Phase Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Phase [degrees]')
p = legend('\gamma = 0.3','\gamma = 0.95','\gamma = 1.05','\gamma = -0.95', 4);
set(p,'FontSize', 8);

%generate transfer function impulse response
[h1_ir, t1] = impz(1, h1);
[h2_ir, t2] = impz(1, h2);
[h3_ir, t3] = impz(1, h3);
[h4_ir, t4] = impz(1, h4);

figure %plot impulse response
plot(t1, h1_ir, 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(t2, h2_ir, 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(t3, h3_ir, 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
plot(t4, h4_ir, 'Color', [0.25 0 1.00], 'LineWidth', 1);
hold on
xlim([0 80])
ylim([-1 10])
title('Impulse Response')
xlabel('Sample [-]')
ylabel('Amplitude [-]')
p = legend('\gamma = 0.3','\gamma = 0.95','\gamma = 1.05','\gamma = -0.95', 1);
set(p,'FontSize', 8);

%%
clear all
clc

%% Recursive filters with a double real pole

%generate transfer function denominators
h1 = poly([0.3, 0.3]);      %gamma =  0.3
h2 = poly([0.95, 0.95]);    %gamma =  0.95
h3 = poly([1.05, 1.05]);    %gamma =  1.05
h4 = poly([-0.95, -0.95]);  %gamma = -0.95

figure %plot z-plane pole diagrams
subplot(2,2,1)
zplane(1, h1);
title('\gamma = 0.3')
subplot(2,2,2)
zplane(1, h2);
title('\gamma = 0.95')
subplot(2,2,3)
zplane(1, h3);
title('\gamma = 1.05')
subplot(2,2,4)
zplane(1, h4);
title('\gamma = -0.95')

%generate transfer function frequency response
[H1, w1] = freqz(1, h1, 512);
[H2, w2] = freqz(1, h2, 512);
[H3, w3] = freqz(1, h3, 512);
[H4, w4] = freqz(1, h4, 512);

figure %plot magnitude and phase response
subplot(2,1,1)
plot(w1/pi, mag2db(abs(H1)), 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(w2/pi, mag2db(abs(H2)), 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(w3/pi, mag2db(abs(H3)), 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
plot(w4/pi, mag2db(abs(H4)), 'Color', [0.25 0 1.00], 'LineWidth', 1);
hold on
title('Magnitude Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Magnitude [dB]')
p = legend('\gamma = 0.3','\gamma = 0.95','\gamma = 1.05','\gamma = -0.95', 1);
set(p,'FontSize', 8);
subplot(2,1,2)
plot(w1/pi, (180/pi)*angle(H1), 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(w2/pi, (180/pi)*angle(H2), 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(w3/pi, (180/pi)*angle(H3), 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
plot(w4/pi, (180/pi)*angle(H4), 'Color', [0.25 0 1.00], 'LineWidth', 1);
hold on
ylim([-210 210])
title('Phase Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Phase [degrees]')
p = legend('\gamma = 0.3','\gamma = 0.95','\gamma = 1.05','\gamma = -0.95', 4);
set(p,'FontSize', 8);

%generate transfer function impulse response
[h1_ir, t1] = impz(1, h1);
[h2_ir, t2] = impz(1, h2);
[h3_ir, t3] = impz(1, h3);
[h4_ir, t4] = impz(1, h4);

figure %plot impulse response
plot(t1, h1_ir, 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(t2, h2_ir, 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(t3, h3_ir, 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
plot(t4, h4_ir, 'Color', [0.25 0 1.00], 'LineWidth', 1);
hold on
xlim([0 80])
ylim([-10 35])
title('Impulse Response')
xlabel('Sample [-]')
ylabel('Amplitude [-]')
p = legend('\gamma = 0.3','\gamma = 0.95','\gamma = 1.05','\gamma = -0.95', 1);
set(p,'FontSize', 8);

%%
clear all
clc

%% Recursive filters with conjugate pole pair

%generate transfer function denominators
h1 = poly([0.75 + 0.40*1i, 0.75 - 0.40*1i]);    %gamma =  0.75 + 0.40i, gamma* =  0.75 - 0.40i
h2 = poly([0.75 + 0.75*1i, 0.75 - 0.75*1i]);    %gamma =  0.75 + 0.75i, gamma* =  0.75 - 0.75i
h3 = poly([-0.75 + 0.40*1i, -0.75 - 0.40*1i]);  %gamma = -0.75 + 0.40i, gamma* = -0.75 - 0.40i

figure %plot z-plane pole diagrams
subplot(2,2,1)
zplane(1, h1);
title('\gamma = 0.75 \pm 0.40i')
subplot(2,2,2)
zplane(1, h2);
title('\gamma = 0.75 \pm 0.75i')
subplot(2,2,3)
zplane(1, h3);
title('\gamma = -0.75 \pm 0.40i')

%generate transfer function frequency response
[H1, w1] = freqz(1, h1, 512);
[H2, w2] = freqz(1, h2, 512);
[H3, w3] = freqz(1, h3, 512);

figure %plot magnitude and phase response
subplot(2,1,1)
plot(w1/pi, mag2db(abs(H1)), 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(w2/pi, mag2db(abs(H2)), 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(w3/pi, mag2db(abs(H3)), 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
title('Magnitude Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Magnitude [dB]')
p = legend('\gamma = 0.75 \pm 0.40i','\gamma = 0.75 \pm 0.75i','\gamma = -0.75 \pm 0.40i', 1);
set(p,'FontSize', 8);
subplot(2,1,2)
plot(w1/pi, (180/pi)*angle(H1), 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(w2/pi, (180/pi)*angle(H2), 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(w3/pi, (180/pi)*angle(H3), 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
ylim([-210 210])
title('Phase Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Phase [degrees]')
p = legend('\gamma = 0.75 \pm 0.40i','\gamma = 0.75 \pm 0.75i','\gamma = -0.75 \pm 0.40i', 4);
set(p,'FontSize', 8);

%generate transfer function impulse response
[h1_ir, t1] = impz(1, h1);
[h2_ir, t2] = impz(1, h2);
[h3_ir, t3] = impz(1, h3);

figure %plot impulse response
plot(t1, h1_ir, 'Color', [1.00 0 0.25], 'LineWidth', 1);
hold on
plot(t2, h2_ir, 'Color', [0.75 0 0.50], 'LineWidth', 1);
hold on
plot(t3, h3_ir, 'Color', [0.50 0 0.75], 'LineWidth', 1);
hold on
xlim([0 80])
ylim([-1 10])
title('Impulse Response')
xlabel('Sample [-]')
ylabel('Amplitude [-]')
p = legend('\gamma = 0.75 \pm 0.40i','\gamma = 0.75 \pm 0.75i','\gamma = -0.75 \pm 0.40i', 4);
set(p,'FontSize', 8);

%%
clear all
clc

%% Effects of varying position (length/angle) of conjugate pole pair

%1. Vary pole placement (length/angle)
%2. Frequency response




%%
clear all
clc

%% 1.2 Placing zeros

%1. Pole-zero placements / Reciprocal transfer function
%2. Magnitude response

%generate denominator/numerator polynomial
h = poly([0.75 + 0.40*1i, 0.75 - 0.40*1i]);    %gamma =  0.75 + 0.40i, gamma* =  0.75 - 0.40i

figure %plot z-plane pole-zero diagrams
subplot(2,1,1)
zplane(1, h);
title('Poles \gamma = 0.75 \pm 0.40i')
subplot(2,1,2)
zplane(h, 1);
title('Zeros \gamma = 0.75 \pm 0.40i')

%generate transfer function frequency response
[H_p, w_p] = freqz(1, h, 512);
[H_z, w_z] = freqz(h, 1, 512);

figure %plot magnitude response
plot(w_p/pi, mag2db(abs(H_p)), 'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
plot(w_z/pi, mag2db(abs(H_z)), 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
title('Magnitude Response')
xlabel('Normalized frequency [\times \pi /sample]')
ylabel('Magnitude [dB]')
p = legend('Poles \gamma = 0.75 \pm 0.40i','Zeros \gamma = 0.75 \pm 0.40i', 1);
set(p,'FontSize', 8);


%%
clear all
clc

%% 2 Making the room IIR

%% 2.1 Bounce bounce ...

%1. Impulse response
%2. Filter coefficients
%3. Frequency response

[s, fs] = audioread(['C:\MATLAB' filesep 'mini-me_short_16bits.wav']);
alpha = 0.6;      %attenuation constant
d = (30/1000)*fs; %DELAY: VARY THIS TO COMPLETE THE LAST TASK (30 ms DEFAULT)

% FIR delay filter with difference equation y[n] = x[n] + alpha * x[n - d]
b_fir = [1, zeros(1, d-1), alpha];
a_fir = 1;
[H_fir, w_fir] = freqz(b_fir, a_fir, 2^16, fs);
[h_fir, t_fir] = impz(b_fir, a_fir);

% IIR delay filter with difference equation y[n] + alpha * y[n - d] = x[n]
b_iir = 1;
a_iir = [1, zeros(1, d-1), alpha];
[H_iir, w_iir] = freqz(b_iir, a_iir, 2^16, fs);
[h_iir, t_iir] = impz(b_iir, a_iir);

%% Plot z-plane pole-zero diagrams

figure 
subplot(2,1,1)
zplane(b_iir,a_iir)
title('Poles IIR delay filter')
subplot(2,1,2)
zplane(b_fir,a_fir)
title('Zeros FIR delay filter')

%% FIR/IIR frequency and impulse responses

% FIR/IIR delay filter impulse response 
figure %plot impulse response
plot(t_iir, h_iir,  'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
plot(t_fir, h_fir,  'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
xlim([-1000 9000])
ylim([-1 1])
title('Impulse Response')
xlabel('Sample [-]')
ylabel('Amplitude [-]')
p = legend('IIR delay filter','FIR delay filter', 4);
set(p,'FontSize', 8);

% FIR/IIR delay filter frequency response 
figure %plot magnitude and phase response
subplot(2,1,1)
plot(w_iir, mag2db(abs(H_iir)), 'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
plot(w_fir, mag2db(abs(H_fir)), 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
%xlim([0 fs/2])
xlim([0 fs/250])
ylim([-10 10])
title('Magnitude Response')
xlabel('Frequency [Hz]')
ylabel('Magnitude [dB]')
p = legend('IIR delay filter','FIR delay filter', 1);
set(p,'FontSize', 8);
subplot(2,1,2)
plot(w_iir, (180/pi)*angle(H_iir), 'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
plot(w_fir, (180/pi)*angle(H_fir), 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
%xlim([0 fs/2])
xlim([0 fs/250])
ylim([-50 50])
title('Phase Response')
xlabel('Frequency [Hz]')
ylabel('Phase [degrees]')
p = legend('IIR delay filter','FIR delay filter', 1);
set(p,'FontSize', 8);

%% Filter signal

s_f_fir = conv(h_fir, s);
s_f_iir = conv(h_iir, s);

%% Play signal

%soundsc(s_f_fir, fs)
%soundsc(s_f_iir, fs)

%% Effects of varying delay on speech signal 

%Vary the delay parameter d in the above code.

%%
clear all
clc

%% 3 Making more of less

%% 3.1 Ex. 12.6 in the book - again

Rp = -2;  %Gp dB
Rs = -11; %Gs dB
Wh = 35;  %rad/s - angular nyquist freq apprx 5.57 Hz
Wp = 10/Wh; %normalized passband corner freq
Ws = 15/Wh; %normalized stopband corner freq

fs = 2*Wh/(2*pi); %sampling freq
Th = 2*pi/Wh; %period

%generate LP Butterworth filter
[n, Wc] = buttord(Wp, Ws, Rp, Rs);
[b, a]  = butter(n, Wc);
[H, w]  = freqz(b, a, 2^16, 2*pi*fs);
[h, t]  = impz(b, a);
wc = Wc*Wh;

%plot frequency response
figure
subplot(2,1,1)
plot(w, abs(H), 'Color', [0.00 0 1.00], 'LineWidth', 1);
xlim([0 Wh])
ylim([0 1.05])
title('LP Butterworth (\omega_c = 10.86 rad/s, n = 3) \\ Magnitude response')
xlabel('Angular frequency [rad/s]')
ylabel('Magnitude [-]')
subplot(2,1,2)
plot(w, (180/pi)*angle(H), 'Color', [0.00 0 1.00], 'LineWidth', 1);
xlim([0 Wh])
ylim([-200 200])
title('LP Butterworth (\omega_c = 10.86 rad/s, n = 3) \\ Phase response')
xlabel('Angular frequency [rad/s]')
ylabel('Phase [degrees]')

%transfer function is b/a (polynomial in negative powers)

%% Plot z-plane pole-zero diagram

figure 
zplane(b, a)
title('LP Butterworth (\omega_c = 10.86 rad/s, n = 3) \\ Pole-Zero diagram')

%% 3.2 The issue of phase shifts and order

%% Zero phase shift filter (using filtfilt)

%create pure tone (10 rad/s)
fs = 2*Wh/(2*pi);
Ts = 5;
t = 0:1/fs:Ts;
w0 = 10;  % < 35 rad/s
f0 = w0/(2*pi);
s = cos(w0*t);

s_f = filter(b, a, s);
s_ff = filtfilt(b, a, s);

figure
subplot(2,1,1)
plot(t, s,  'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
plot(t, s_ff, 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
xlim([0 5])
ylim([-1 1])
title('Pure tone (10 rad/s) filtered with zero phase shift (filtfilt)')
xlabel('Time [s]')
ylabel('Amplitude [-]')
p = legend('Original signal', 'Filtered signal', 1);
set(p,'FontSize', 8);
subplot(2,1,2)
plot(t, s,  'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
plot(t, s_f, 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
xlim([0 5])
ylim([-1 1])
title('Pure tone (10 rad/s) filtered with non-zero phase shift (filter)')
xlabel('Time [s]')
ylabel('Amplitude [-]')
p = legend('Original signal', 'Filtered signal', 1);
set(p,'FontSize', 8);

%% Higher order filter

%higher filter order can be obtained via series connection/cascade
H_1 = H;
H_2 = H.*H;
H_3 = H.*H.*H;

figure %plot frequency response
subplot(2,1,1)
plot(w, abs(H_1), 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
plot(w, abs(H_2), 'Color', [0.50 0 0.50], 'LineWidth', 1);
hold on
plot(w, abs(H_3), 'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
xlim([0 Wh])
ylim([0 1.05])
title('Magnitude response')
xlabel('Angular frequency [rad/s]')
ylabel('Magnitude [-]')
p = legend('Original', 'Two in series', 'Three in series', 1);
set(p,'FontSize', 8);
subplot(2,1,2)
plot(w, (180/pi)*angle(H_1), 'Color', [0.00 0 1.00], 'LineWidth', 1);
hold on
plot(w, (180/pi)*angle(H_2), 'Color', [0.50 0 0.50], 'LineWidth', 1);
hold on
plot(w, (180/pi)*angle(H_3), 'Color', [1.00 0 0.00], 'LineWidth', 1);
hold on
xlim([0 Wh])
ylim([-200 200])
title('Phase response')
xlabel('Angular frequency [rad/s]')
ylabel('Phase [degrees]')
p = legend('Original', 'Two in series', 'Three in series', 4);
set(p,'FontSize', 8);

%% END



