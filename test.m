%% Transfer function test

clear all
clc

%% Presets

%generate LP Butterworth filter
Rp = -2;  %Gp dB
Rs = -11; %Gs dB
Wh = 35;  %rad/s - angular nyquist freq apprx 5.57 Hz
Wp = 10/Wh; %normalized passband corner freq
Ws = 15/Wh; %normalized stopband corner freq
[n, Wc] = buttord(Wp, Ws, Rp, Rs);
[b, a]  = butter(n, Wc);

%pure tone
T  = 1;
N0 = 150;
t = 0:1/N0:T-1/N0;

q = 100;
x = sin(q*t);

%% Time-reversal and conjugation

z = zeros(1, N0-1);
for k=1:N0
z(k) = exp(2*pi*1i*(k-1)/N0); 
end

A = z.*conj(fft(x));
B = fft(fliplr(x));

figure
subplot(2,1,1)
plot(t, abs(A), 'r');
subplot(2,1,2)
plot(t, abs(B), 'b');

m = sumabs(abs(A-B))

%% Tranfer function sampling points

[H, w] = freqz(b, a, N0, 'whole');
H = H';

Z = ifft(conj(H).*H.*fft(x));
F = filtfilt(b, a, x);

figure
subplot(3, 1, 1)
plot(t, Z, 'r');
subplot(3, 1, 2)
plot(t, F, 'b');
subplot(3, 1, 3)
plot(t, Z - F, 'k');

m = sumabs(abs(Z-F))
