function [ signal ] = generate_ramp( T0, t, fs )
%GENERATE_RAMP Summary of this function goes here
%   Detailed explanation goes here
num_zeros = (t-T0)*fs
signal = [0:1/fs:T0, zeros(1,num_zeros)];

end

