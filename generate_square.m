function [ signal ] = generate_square( T0, t, fs )
%GENERATE_SQUARE Summary of this function goes here
%   Detailed explanation goes here
num_ones = T0*fs;
signal = [ones(1,num_ones+1)/T0, zeros(1,t*fs-num_ones)];
end

