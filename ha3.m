%% 1.1 Toolbox: Spectrum
% Sinusoid generation
fs = 10e3; %10kHz sampling rate
f = 1e3; %1kHz frequency
T = 1; %1s period
A = 1; %Amplitude of 1
[t,y] = sine( A, f, 0, fs, T);
% Get the spectrum
[Y,freq] = make_spectrum( y, fs );
%plot power density spectrum
figure(1)
%define parameters for export
set(gcf, 'paperunits','centimeters','Paperposition',[0,0,10,4]);
h1 = plot(freq,abs(Y))

%cosmetics
set(h1, 'linewidth', 1);
set(gca,'Fontsize', 10);
title('f_s = 10kHz');
ylim([0, 0.7]);
xlim([-3000,3000]);
xlabel('frequency [Hz]')
ylabel('amplitude');
saveas(gcf, './pics/3-1-1.eps','psc2');

%% 1.2 Fourier transform of a synthetic signal
% Synthesizing the signal s[t]
T = 2; %period of 2s
N = 10; %sum limit
f0 = 100; %lowest frequency in generated signal
%Highest frequency is 2^N*f0=1024*100=1.024e5, choosing fs 4 times that
fs = 2^N*f0*4;
t = [0:1/fs:T-1/fs]; %time vector

% Generate signal
s = zeros(1,length(t));
for k = 0:N;
    s = s + cos(2*pi*2^k*f0*t+k*pi/3);
end

figure(1)
% plot s[0.9s,0.93s]
set(gcf, 'paperunits','centimeters','Paperposition',[0,0,15,4]);
h1 = plot(t,s); 
%cosmetics
set(h1, 'linewidth', 1);
set(gca,'Fontsize', 10);
title('s[t]');
ylim([-8,8]);
xlim([0.9,0.93]);
xlabel('time [s]')
ylabel('amplitude');
saveas(gcf, './pics/3-1-2.eps','psc2');


figure(2)
set(gcf, 'paperunits','centimeters','Paperposition',[0,0,15,8]);
% spectrum of s
[Y,freq] = make_spectrum(s,fs);
subplot(2,1,1);
%positive frequencies magnitude
h1 = plot(freq(1:ceil(length(freq)/2)),2*abs(Y(1:(ceil(length(Y)/2)))))
%cosmetics
set(h1, 'linewidth', 1);
set(gca,'Fontsize', 10);
title('Magnitude spectrum')
ylim([0,1.1]);
xlim([0,105000]);
xlabel('Frequency [Hz]')
ylabel('magnitude');
% all frequencies magnitude
%subplot(2,2,2);
%plot(fftshift(freq),2*abs(fftshift(Y)))
%real part
subplot(2,2,3);
h2 = plot(freq(1:ceil(length(freq)/2)),real(2*Y(1:(ceil(length(Y)/2))))) %only pos freqs
%cosmetics
title('Real part');
ylim([-1.1,1.1]);
xlim([0,105000]);
xlabel('Frequency [Hz]')
ylabel('amplitude');
%im part
subplot(2,2,4);
h3 = plot(freq(1:ceil(length(freq)/2)),imag(2*Y(1:(ceil(length(Y)/2))))) %only pos freqs
%cosmetics
title('Imag part');
ylim([-1.1,1.1]);
xlim([0,105000]);
xlabel('Frequency [Hz]')
ylabel('amplitude');
saveas(gcf, './pics/3-1-3.eps','psc2');

% log dB plot
figure(3)
set(gcf, 'paperunits','centimeters','Paperposition',[0,0,15,5]);
semilogx(freq(1:ceil(length(freq)/2)),10*log10(2*abs(Y(1:(ceil(length(Y)/2)))))) %extract only pos freqs
%make the red dot on first peak
[pks, locs] = findpeaks(10*log10(2*abs(Y(1:(ceil(length(Y)/2))))), 'MINPEAKHEIGHT', -10);
hold on
plot(freq(locs(1)),pks(1),'or', 'MarkerSize', 15);
hold off
%cosmetics
set(gca, 'XTick', logspace(0, 5,6));
title('s magnitude spectrum in dB');
ylim([-160,50]);
xlim([0,150000]);
xlabel('Frequency [Hz]')
ylabel('magnitude [dB]');
saveas(gcf, './pics/3-1-4.eps','psc2');

%spectrum range 395-405
figure(4)
set(gcf, 'paperunits','centimeters','Paperposition',[0,0,10,4]);
plot(freq,2*abs(Y)); xlim([395,405]);
line([400;400],[0;1.4],'LineStyle','--','Color', 'r')
title('s magnitude spectrum');
ylim([0,1.1]);
xlabel('Frequency [Hz]')
ylabel('magnitude');
saveas(gcf, './pics/3-1-5.eps','psc2');

% save s to ha3_s_12.wav
NBits = 16;
name = 'ha3_s_12.wav';
wavwrite(s/max(abs(s)),fs,NBits,name); %s has to be scaled to between -1 and 1 or it will be clipped
%read it back in
s_wav = wavread(name)'*max(abs(s)); %convert to row vector and scale to orignal
%plot and compare with original
figure(1)
hold on
plot(t,s_wav,'r'); xlim([0.9,0.93]);
line([400;400],[0;1.4],'LineStyle','--','Color', 'r')
title('s original vs. wav readin');
ylim([-8,8]);
xlabel('time [s]')
ylabel('amplitude');
legend('org. signal','.wav file');
saveas(gcf, './pics/3-1-6.eps','psc2');
hold off

%% 1.3 Fourier transform of a recorded signal
%read the file to get data, bitrate and sampling frequency
[piano, fs, NBits] = wavread('piano.wav'); 
piano = piano'; %convert to row vector
T = (length(piano))/fs;
t = [0:1/fs:T-1/fs];

%plot the piano waveform in range [0,1]
figure(1)
plot(t,piano); xlim([0,1]);

%generate spectrum
[Y,freq] = make_spectrum(piano,fs);
%scale signal such that max value=1. Necessary because the wavefile is
%normalised giving a skewed spectrum?
%Y = Y/max(Y); 
%plot spectrum
figure(3); subplot(1,1,1) %clear previous section subplots
plot(freq(1:ceil(length(freq)/2)),10*log10(abs(Y(1:(ceil(length(Y)/2)))))) %extract only pos freqs
%fundamental freq read as 130.5Hz, C3 has frequency 130.8Hz, close enough.
figure(2)
%complex spectrum
subplot(2,1,1);
plot(fftshift(freq),fftshift(real(Y)));
xlim([-2000,2000]);
subplot(2,1,2);
plot(fftshift(freq),fftshift(imag(Y)));
xlim([-2000,2000]);
%Synthesizing in the fourier domain
%coefficients to be placed on harmonics, read from piano.wav spectrum
coeff = [0.006481-0.006438i,-0.002627-0.003091i,0.005457+0.005018i,-0.001769-0.001969i,0.002483+0.002404i,-0.001428-0.001399i,0.001044+0.001066i,0.0003612-0.0004801i,0.003743+0.002712i,0.001508+0.00191i];
%coeff = real(coeff);
%generate frequency vector for signal with 2s period and samplerate of
%44100, starting with positive freqs only to assign the coefficients and
%then mirror it around
T=2;
freqs = [0:1/T:fs/2-1/T];
Ys = zeros(1,length(freqs));
f0=130.8;
for c = 1:length(coeff)
    % find index of closest element to c*130.8Hz
    [idx, idx] = min(abs(freqs-c*f0));
    %put coeff(c) in that spot
    Ys(idx)=coeff(c);
end
%mirror the positive frequencies into negatives
freqs = [freqs,-fs/2,flip(freqs(2:end))*(-1)];
%Do the same for the spectrum values
Ys = [Ys,0,conj(flip(Ys(2:end)))];
%plot the spectrum
figure(4)
subplot(2,1,2)
plot(fftshift(freqs),abs(fftshift(Ys)))
hold on
%compare with original piano spectrum
plot(fftshift(freq),abs(fftshift(Y)),'r')
hold off
%invser fourier transform
s = ifft(Ys);
% scale s back to original signal
s = s/max(abs(s));
%generate corresponding time vector
t = [0:1/fs:T-1/fs];
%plot the signal
subplot(2,1,1);
plot(t,s);
%% 2 Low bitrate telephone transmission
% 3rd order bspline with n=6
h3 = make_bspline(3,6);
%load spoken sentence and it's sampling frequency
[spoken, fs, NBits] = wavread('spoken_sentence_44100.wav');
spoken = spoken'; %we like row vectors

%Finding attenuation at 4 and 5kHz
[Yh,freqh] = make_spectrum(zero_pad(h3, 2, fs), fs); %extend spline to 2 seconds to get good fft
%scale 
Yh = Yh/max(Yh);
figure(2)
subplot(1,1,1);
%plot(fftshift(freqh),fftshift(abs(Yh(1:end-1))));
semilogx(freqh(1:ceil(length(freqh)/2)),10*log10(abs(Yh(1:(ceil(length(Yh)/2)-1))))) %extract only pos freqs

%2kHz->0.6153, 4kHz->0.1189, 5kHz->0.0264

% Spectrum of the audio signal
[Ys, freqs] = make_spectrum(spoken,fs);
figure(3)
%plot(fftshift(freqs),fftshift(2*abs(Ys)))
semilogx(freqs(1:ceil(length(freqs)/2)),10*log10(abs(Ys(1:(ceil(length(Ys)/2)))))) %extract only pos freqs

%convolve h3 and spoken sentence
fspoken = conv(h3,spoken);
[Yf,freqf] = make_spectrum(fspoken,fs); %spectrum of filtered signal
hold on
%plot(fftshift(freqf),fftshift(2*abs(Yf)),'r')
semilogx(freqf(1:ceil(length(freqf)/2)),10*log10(abs(Yf(1:(ceil(length(Yf)/2))))),'r') %extract only pos freqs
hold off

%Downsample
fspoken_down = fspoken(1:7:end);
fs_down = fs/7; %new frequency
[Yf_down,freqf_down] = make_spectrum(fspoken_down, fs_down); %spectrum of downsampled signal
figure(4)
semilogx(freqf_down(1:ceil(length(freqf_down)/2)),10*log10(abs(Yf_down(1:(ceil(length(Yf_down)/2)))))) %extract only pos freqs


%Upsample the downsample with zero order hold
fspoken_up = reshape(ones(7,1)*fspoken_down(:)',1,length(fspoken_down)*7);
%get the spectrum
[Yf_up,freqf_up] = make_spectrum(fspoken_up,fs);
figure(1)
%plot positive frequencies dB/log scale
semilogx(freqf_up(1:ceil(length(freqf_up)/2)),10*log10(abs(Yf_up(1:(ceil(length(Yf_up)/2)))))) %extract only pos freqs

%low pass filter with same bspline to remove it's zeroes, maybe not?
h5 = make_bspline(5,6);
fspoken_upf = conv(fspoken_up,h5);
%get the spectrum
[Yf_upf,freqf_upf] = make_spectrum(fspoken_upf,fs);
hold on
%plot positive frequencies dB/log scale and compare with unfiltered
%upsample
semilogx(freqf_upf(1:ceil(length(freqf_upf)/2)),10*log10(abs(Yf_upf(1:(ceil(length(Yf_upf)/2))))),'r') %extract only pos freqs
hold off